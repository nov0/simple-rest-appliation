var gulp = require('gulp');
var gulpConcat = require('gulp-concat');


gulp.task('scripts', function () {
    gulp.src('./src/main/resources/static/app/**/*.js')
        .pipe(gulpConcat('app.min.js'))
        .pipe(gulp.dest('./src/main/resources/static/build/'));
});

gulp.task('dev', function () {
    gulp.start(['scripts']);
    gulp.watch(['./src/main/resources/static/app/**/*.js'], ['scripts']);
});

gulp.task('default', ['scripts']);