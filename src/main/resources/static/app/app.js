'use strict'
angular.module('app', [
    'app.home',
    'ngRoute'

    ])
    .config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
    }])
    .constant('REST_ENDPOINT', location.protocol + '//' + location.host);