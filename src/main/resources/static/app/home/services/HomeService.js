'use strict'
angular.module('app').service('HomeService', ['$http', 'REST_ENDPOINT', function ($http, REST_ENDPOINT) {
    return {
        initialize: function() {
            return $http.get(REST_ENDPOINT + '/initialize').then(function(response) {
                return response.data;
            }, function() {
                console.log("Errror");
            })
        }
    }
}]);