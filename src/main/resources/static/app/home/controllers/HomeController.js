angular.module('app').controller('HomeController', ['$scope', 'HomeService', function ($scope, HomeService) {
    $scope.welcome = "Hello world!!";
    $scope.links = [];

    $scope.getInitializedLinks = function() {
        HomeService.initialize().then(function(data) {
            $scope.links = data;
        }, function () {
            console.log("Error in home controller while initializing data");
        })
    };

    $scope.getInitializedLinks();

}]);