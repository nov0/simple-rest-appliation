angular.module('app.home', ['ngRoute', 'ngResource']).config(['$routeProvider', function($routeProvider) {
     $routeProvider
         .when('/', {
            templateUrl: 'app/home/views/home.html',
            controller: 'HomeController'
         });
}]);