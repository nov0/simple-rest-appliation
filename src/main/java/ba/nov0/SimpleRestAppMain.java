package ba.nov0;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleRestAppMain {
    public static void main(String[] args) {
        SpringApplication.run(SimpleRestAppMain.class, args);
    }
}
