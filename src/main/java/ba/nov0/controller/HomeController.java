package ba.nov0.controller;

import ba.nov0.service.movie.MovieService;
import ba.nov0.service.user.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    private static final Logger LOG = LogManager.getLogger(HomeController.class);
    private static final String HOME_MESSAGE = "Sending response to home.";


    @Value("${application.address.url}")
    private String serverAddress;

    private final UserService userService;
    private final MovieService movieService;

    @Autowired
    public HomeController(UserService userService, MovieService movieService) {
        this.userService = userService;
        this.movieService = movieService;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping("/initialize")
    public ResponseEntity initializeUsersAndMovies() {
        userService.initializeUsers();
        movieService.initialize();
        return new ResponseEntity(getResponseMessage(), HttpStatus.OK);
    }

    private Map<String, String> getResponseMessage() {
        Map<String, String> responseMessage = new HashMap<>();
        responseMessage.put("users", "initialized");
        responseMessage.put("movies", "initialized");
        responseMessage.put("getAllMovie", serverAddress + "movies/getAllMovies");
        responseMessage.put("getMovie", serverAddress + "movies/getMovie?id=3");
        responseMessage.put("getAllUsers", serverAddress + "users/getAllUsers");
        responseMessage.put("getUser", serverAddress + "users/getUser?id=2");
        return responseMessage;
    }
}
