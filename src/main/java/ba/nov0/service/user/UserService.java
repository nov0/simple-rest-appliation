package ba.nov0.service.user;

import ba.nov0.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    void initializeUsers();
    List<User> getAllUsers();
    User getUser(Integer id);

}
