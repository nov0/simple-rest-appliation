package ba.nov0.service.user;

import ba.nov0.domain.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("userService")
public class UserServiceImpl implements UserService {

    private static String EMAIL_TEMPLATE = "email%s@domain.com";
    private static List<User> users = new ArrayList<>();

    @Override
    public void initializeUsers() {

        if (users.isEmpty()) {
            for (int i = 1; i <= 10; i++) {
                User user = new User();
                user.setId(i);
                user.setFirstName("firstName" + i);
                user.setLastName("lastName" + i);
                user.setEmail(String.format(EMAIL_TEMPLATE, i));
                user.setUsername("username" + i);
                user.setPassword("strongPassword" + 1);
                users.add(user);
            }
        }
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public User getUser(Integer id) {
        return getUserById(id);
    }

    private User getUserById(Integer id) {
        for (User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }
}
