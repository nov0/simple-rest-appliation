package ba.nov0.service.movie;

import ba.nov0.domain.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MovieService {

    void initialize();

    List<Movie> getAllMovies();

    Movie getMovie(Integer id);
}
