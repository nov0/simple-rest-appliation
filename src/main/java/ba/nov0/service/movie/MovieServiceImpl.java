package ba.nov0.service.movie;

import ba.nov0.domain.Movie;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("moveService")
public class MovieServiceImpl implements MovieService {

    private static List<Movie> movies = new ArrayList<>();

    @Override
    public void initialize() {
        if (movies.isEmpty()) {
            for (int i = 1; i <= 5; i++) {
                Movie movie = new Movie();
                movie.setId(i);
                movie.setName("movieName" + i);
                movie.setDirectors("directors" + i);
                movie.setYear(2000 + i);
                movies.add(movie);
            }
        }
    }

    @Override
    public List<Movie> getAllMovies() {
        return movies;
    }

    @Override
    public Movie getMovie(Integer id) {
        return getMovieById(id);
    }

    private Movie getMovieById(Integer id) {
        for (Movie movie : movies) {
            if (movie.getId().equals(id)) {
                return movie;
            }
        }
        return null;
    }
}
